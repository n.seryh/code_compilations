# Code_compilations

A bunch of ways for compiling different codes on different machines

For general infomation about aims compilation visit:
https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial

Compilation of aims locally using ninja is discussed here: https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/Compiling-FHI-aims-in-a-Conda-environment

Some notes for compilations are also available here: https://aims-tools.readthedocs.io/en/latest/intros/aims_env/compiling_aims.html
