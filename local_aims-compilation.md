This guide compiles FHI-aims such that all the prerequisites (build tools, compilers, and libraries) are provided by Conda in an approximately isolated environment. It should work on both Linux and MacOS.

1. Make sure you have a working Conda installation, for instance by [installing Miniconda](https://docs.conda.io/en/latest/miniconda.html).
1. Obtain a source distribution of FHI-aims or clone the FHI-aims Git repository, and change to the root directory of the source tree.
1. Run the following commands:
    ```
    conda create -n fhi-aims
    conda activate fhi-aims
    conda install -c conda-forge \
      cmake ninja "gfortran<10" "clang<12" openmpi-mpifort scalapack
    mkdir build
    cd build/
    cmake .. -G Ninja \
      -DCMAKE_Fortran_COMPILER=mpifort \
      -DCMAKE_Fortran_FLAGS="-O3 -ffree-line-length-none" \
      -DFortran_MIN_FLAGS="-O0 -ffree-line-length-none" \
      -DCMAKE_C_FLAGS="-O3" \
      -DLIBS="lapack scalapack" \
      -DCMAKE_Fortran_PREPROCESS=OFF \
      -DCMAKE_PREFIX_PATH=$CONDA_PREFIX
    ninja
    ```

## Notes

- [Conda](https://docs.conda.io/) is a package, dependency, and environment management system.
- This setup is intended for quick testing or for development. For production-level HPC calculations, FHI-aims should be built with performance-oriented compilers and libraries.
- When developing, it is a good practice to maintain all project dependencies locally in an isolated environment.
- The meaning of the options passed to Cmake in the command above, as well as other possible choices, can be found in the [Cmake tutorial](CMake-Tutorial).
- [Ninja](https://ninja-build.org) is a non-default build system, supported by CMake, which is usually faster than Make. By default, it preprocesses each Fortran source file (unlike Make), which is undesirable when building FHI-aims, hence the `CMAKE_Fortran_PREPROCESS=OFF` option.
- Setting the `CMAKE_PREFIX_PATH` variable makes sure that CMake looks for compilers and libraries preferentially in that prefix tree.
- Using Clang 12 on MacOS crashes with an ICE.
- Gfortran 10 would require passing `-fallow-argument-mismatch`.
