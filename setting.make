FC=ifort
FFLAGS=-O3 -ip -xHost -fp-model precise -module $(MODDIR)
F90FLAGS=$(FFLAGS)
ARCHITECTURE=Generic
USE_MPI=yes
MPIFC=mpiifort
LAPACKBLAS=-L${MKLROOT}/lib/intel64/ -Wl,--start-group -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -Wl,--end-group -lpthread
SCALAPACK=-L${MKLROOT}/lib/intel64/ -Wl,--start-group -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -Wl,--end-group
CC=gcc
CCFLAGS=-c
MPIF=
USE_SPGLIB=yes
USE_C_FILES=yes
include Makefile
